from api.services.update_order_service import TestUpdateOrderService
from ui.actors.portfolio_actor import PortfolioActor


class ApiActor:
    def __init__(self, driver):
        self.get_order_service = TestUpdateOrderService()
        self.portfolio_actor = PortfolioActor(driver)

    def update_order_status(self, user):
        self.get_order_service.test_update_order_status(user)

        return self.portfolio_actor
