import allure
import requests

from api.services.get_tokens_service import GetTokensService
from ui.config.config import config


class TestUpdateOrderService:
    @staticmethod
    def prepare_request_headers(refresh_token):
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer " + refresh_token.access_token,
        }

        return headers

    @staticmethod
    def get_last_order_id(user):
        with allure.step('Getting last order id'):
            user_refresh_token = GetTokensService.login()
            url = config.API_URL + ("/v1/admin/orders/?order_by=-deal_opening_date%2C-created_at&client__order_by"
                                    f"=-registered_at&client__email={user.user_email}&limit=1&offset=0")

            headers = {
                "accept": "application/json",
                "Authorization": "Bearer " + user_refresh_token.access_token,
            }

            response = requests.get(url=url, headers=headers)

            assert response.status_code == 200

            user_dict = response.json()['data']
            order_id = user_dict[0]['order_id']

            return order_id

    def update_order_status(self, user):
        with allure.step('Updating order status'):
            last_order_id = self.get_last_order_id(user)
            url = config.API_URL + f'/v2/admin/orders/{last_order_id}'
            request_body = {
                "status": "Transaction Completed"
            }

            headers = self.prepare_request_headers(GetTokensService.login())
            response = requests.patch(url=url, headers=headers, json=request_body)

            assert response.status_code == 200

            updated_order = response.json()

            return updated_order
