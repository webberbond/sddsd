import allure
import requests

from ui.config.config import config
from ui.entities.models.user_tokens import UserTokens


class GetTokensService:
    @staticmethod
    def login() -> UserTokens:
        with allure.step("Login to admin panel using API"):
            request_body = {
                "password": config.PASSWORD,
                "email": config.EMAIL
            }

            url = config.API_URL + "/v1/authentication-admin/sign-in"
            response = requests.post(url=url, json=request_body)

            assert response.status_code == 200

            dictionary = response.json()
            tokens = UserTokens.as_user_token(dictionary)

            return tokens

    @staticmethod
    def refresh_token(login) -> UserTokens:
        with allure.step("Getting refresh token"):
            request_body = {
                "refresh_token": login.refresh_token
            }

            url = config.API_URL + "/v1/authentication-admin/refresh"
            response = requests.post(url=url, json=request_body)
            dictionary = response.json()
            login.access_token = dictionary['access_token']

            return login.access_token
