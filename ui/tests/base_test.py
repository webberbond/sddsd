import base64
import unittest

import allure
from appium.webdriver.appium_service import AppiumService

from api.actors.api_actor import ApiActor
from ui.actors.login_actor import LoginActor
from ui.base.driver_manager import DriverManager
from ui.pages.portfolio_pages.portfolio_page import PortfolioPage


class BaseTest(unittest.TestCase):
    def setUp(self) -> None:
        with allure.step('Initializing appium server, appium driver, starting screen recording'):
            self.appium_service = AppiumService()
            self.appium_service.start()
            self.driver = DriverManager().get_driver()
            self.driver.start_recording_screen()

            self.login_actor = LoginActor(self.driver)
            self.portfolio_page = PortfolioPage(self.driver)
            self.api_actor = ApiActor(self.driver)

    def tearDown(self) -> None:
        with allure.step('Stopping appium server, quitting appium driver'):
            if self.driver:
                self.stop_screen_recording()
                self.driver.quit()
            self.appium_service.stop()

    def start_screen_recording(self):
        self.driver.start_recording_screen()

    def stop_screen_recording(self):
        data = self.driver.stop_recording_screen()
        allure.attach(name='fileName', body=base64.b64decode(data), attachment_type=allure.attachment_type.MP4)
