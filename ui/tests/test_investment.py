import allure

from ui.entities.datafaker.account_model_data_faker import UserFabric
from ui.entities.models.ideas_model import IdeasModel
from ui.tests.base_test import BaseTest


class TestInvestment(BaseTest):
    @allure.title('Test Investment By Bank With Commission Included')
    @allure.description('User authorization, verification, making order, changing order status')
    def test_investment_by_bank(self):
        user = UserFabric.get_new_random_user()
        kyc_actor = self.login_actor.create_account(user)
        ideas_actor = kyc_actor.proceed_kyc(user)
        investment_actor = ideas_actor.invest_in(IdeasModel.stripe_idea)
        investment_actor.invest_with_commission_included('10000') \
                        .check_spv_document() \
                        .proceed_to_payment() \
                        .select_payment_method('Bank') \
                        .confirm_bank_payment()
        portfolio_actor = self.api_actor.update_order_status(user)
        portfolio_actor.open_stripe_order_page()

        self.assertTrue(self.portfolio_page.is_investment_completed_text_displayed(),
                        "Checking if the order status was changed to 'Transaction Completed'")

    @allure.title('Test Investment By Bank With Commission Included')
    @allure.description('User authorization, verification, making order, changing order status')
    def test_investment_by_bank(self):
        user = UserFabric.get_new_random_user()
        kyc_actor = self.login_actor.create_account(user)
        ideas_actor = kyc_actor.proceed_kyc(user)
        investment_actor = ideas_actor.invest_in(IdeasModel.stripe_idea)
        investment_actor.invest_with_commission_included('10000') \
            .check_spv_document() \
            .proceed_to_payment() \
            .select_payment_method('Bank') \
            .confirm_bank_payment()
        portfolio_actor = self.api_actor.update_order_status(user)
        portfolio_actor.open_stripe_order_page()

        self.assertTrue(self.portfolio_page.is_investment_completed_text_displayed(),
                        "Checking if the order status was changed to 'Transaction Completed'")
