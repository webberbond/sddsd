from appium.webdriver.common.appiumby import AppiumBy

from ui.pages.base_page import BasePage


class PortfolioPage(BasePage):
    close_modal_window_button = 'closeModalWindowButton'
    stripe_order_button = '//android.view.View[contains(@content-desc, "Stripe")]'
    go_to_portfolio_button = '//android.view.View[@content-desc="Go to portfolio"]'
    investment_completed_text = 'Congratulations! Your investment is completed'

    def click_close_modal_window_button(self):
        self.click_element_by_id(self.close_modal_window_button)

    def click_stripe_order_button(self):
        self.click_element_by_xpath(self.stripe_order_button)

    def is_investment_completed_text_displayed(self):
        return self.get_element(AppiumBy.ACCESSIBILITY_ID, self.investment_completed_text).is_displayed()
