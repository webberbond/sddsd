from ui.pages.base_page import BasePage


class RegisterPage(BasePage):
    user_name_input_field = 'userNameField'
    user_email_input_field = 'userEmailField'
    create_account_button = 'createAccountButton'

    def send_keys_to_user_name_input_field(self, user_name):
        self.send_keys_to_element(self.user_name_input_field, user_name)

    def send_keys_to_user_email_input_field(self, user_email):
        self.send_keys_to_element(self.user_email_input_field, user_email)

    def click_create_account_button(self):
        self.click_element_by_id(self.create_account_button)
