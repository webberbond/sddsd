from ui.pages.base_page import BasePage


class OnboardingPage(BasePage):
    onboarding_next_button1 = 'onboardingNextButton1'
    onboarding_next_button2 = 'onboardingNextButton2'
    onboarding_next_button3 = 'onboardingNextButton3'

    def click_onboarding_next_button1(self):
        self.click_element_by_id(self.onboarding_next_button1)

    def click_onboarding_next_button2(self):
        self.click_element_by_id(self.onboarding_next_button2)

    def click_onboarding_next_button3(self):
        self.click_element_by_id(self.onboarding_next_button3)
