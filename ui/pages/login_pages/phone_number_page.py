from ui.pages.base_page import BasePage


class PhoneNumberPage(BasePage):
    country_dropdown_button = '//android.widget.ImageView[contains(@content-desc, "+7")]'
    country_search_field = '//android.widget.EditText'
    my_country_button = '//android.view.View[contains(@content-desc, "Uzbekistan")]'
    phone_number_input_field = 'phoneNumberInputField'
    send_code_button = 'sendCodeButton'

    def click_country_dropdown_button(self):
        self.click_element_by_xpath(self.country_dropdown_button)

    def send_keys_to_country_search_field(self, country_name):
        self.send_keys_to_element_by_xpath(self.country_search_field, country_name)

    def click_my_country_button(self):
        self.click_element_by_xpath(self.my_country_button)

    def send_keys_to_phone_number_input_field(self, phone_number):
        self.send_keys_to_element(self.phone_number_input_field, phone_number)

    def click_send_code_button(self):
        self.click_element_by_id(self.send_code_button)
