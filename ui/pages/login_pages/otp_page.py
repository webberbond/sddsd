from appium.webdriver.extensions.android.nativekey import AndroidKey

from ui.pages.base_page import BasePage


class OtpPage(BasePage):
    def click_first_confirmation_code_digit(self):
        self.enter_key(AndroidKey.DIGIT_1)

    def click_second_confirmation_code_digit(self):
        self.enter_key(AndroidKey.DIGIT_1)

    def click_third_confirmation_code_digit(self):
        self.enter_key(AndroidKey.DIGIT_1)

    def click_fourth_confirmation_code_digit(self):
        self.enter_key(AndroidKey.DIGIT_1)
