from ui.pages.base_page import BasePage


class NavigationBar(BasePage):
    home_navigation_bar_button = 'homeNavigationBarButton'
    portfolio_navigation_bar_button = 'homeNavigationBarButton'
    news_navigation_bar_button = 'newsNavigationBarButton'
    profile_navigation_bar_button = 'profileNavigationBarButton'

    def click_home_navigation_bar_button(self):
        self.click_element_by_id(self.home_navigation_bar_button)

    def click_portfolio_navigation_bar_button(self):
        self.click_element_by_id(self.portfolio_navigation_bar_button)

    def click_news_navigation_bar_button(self):
        self.click_element_by_id(self.news_navigation_bar_button)

    def click_profile_navigation_bar_button(self):
        self.click_element_by_id(self.profile_navigation_bar_button)
