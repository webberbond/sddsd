from ui.pages.base_page import BasePage


class InvestmentPage(BasePage):
    close_story_button = '//android.widget.Button'
    close_modal_window_button = '//android.view.View[contains(@content-desc, "Deep-dive")]/android.view.View[1]'

    def click_close_story_button(self):
        self.click_element_by_xpath(self.close_story_button)

    def click_close_modal_window_button(self):
        self.click_element_by_xpath(self.close_modal_window_button)

    def click_idea_to_invest_button(self, idea_id):
        self.click_element_by_xpath(idea_id)
