from ui.pages.base_page import BasePage


class BankTransferPage(BasePage):
    sent_the_funds_button = 'sentTheFundsButton'
    go_to_portfolio_button = 'goToPortfolioButton'

    def scroll_to_sent_the_funds_button(self):
        self.scroll_gesture(555, 1777, 550, 325, 1000)

    def click_sent_the_funds_button(self):
        self.click_element_by_id(self.sent_the_funds_button)

    def click_go_to_portfolio_button(self):
        self.click_element_by_id(self.go_to_portfolio_button)
