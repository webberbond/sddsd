from ui.pages.base_page import BasePage


class IdeaInvestmentPage(BasePage):
    invest_button = 'investButtonInIdeaButton'

    def click_invest_button(self):
        self.click_element_by_id(self.invest_button)
