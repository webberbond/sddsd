from ui.pages.base_page import BasePage


class PaymentPage(BasePage):
    swift_button = 'swiftButton'

    def click_swift_button(self):
        self.click_element_by_id(self.swift_button)
