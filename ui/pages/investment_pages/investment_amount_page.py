from ui.pages.base_page import BasePage


class InvestmentAmountPage(BasePage):
    investment_amount_input_field = 'investmentAmountInputField'
    continue_button = 'continueToInvestButton'

    def send_keys_to_investment_amount_input_field(self, amount):
        self.send_keys_to_element(self.investment_amount_input_field, amount)

    def click_continue_button(self):
        self.click_element_by_id(self.continue_button)
