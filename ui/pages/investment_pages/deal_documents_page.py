from appium.webdriver.common.appiumby import AppiumBy

from ui.pages.base_page import BasePage


class DealDocumentsPage(BasePage):
    checkbox_documents_button = 'checkboxButton'
    sign_documents_button = 'signInDocumentsButton'
    spv_agreement_button = 'SPV Agreement'

    def click_checkbox_documents_button(self):
        self.click_element_by_id(self.checkbox_documents_button)

    def click_sign_documents_button(self):
        self.click_element_by_id(self.sign_documents_button)

    def is_spv_agreement_button_visible(self):
        return self.get_element(AppiumBy.ACCESSIBILITY_ID, self.spv_agreement_button).is_displayed()
