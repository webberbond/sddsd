from ui.pages.base_page import BasePage


class ProceedWithPaymentPage(BasePage):
    proceed_with_payment_button = 'proceedWithPaymentButton'

    def click_proceed_with_payment_button(self):
        self.click_element_by_id(self.proceed_with_payment_button)
