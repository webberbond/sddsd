import time

import allure
from appium.webdriver import WebElement
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.webdriver import WebDriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    driver: WebDriver

    def __init__(self, driver):
        self.driver = driver

    def is_element_exist(
            self,
            by: str = AppiumBy.ID,
            value: str | dict | None = None,
            wait_for_duration: int = 15
    ):
        try:
            WebDriverWait(self.driver, wait_for_duration).until(
                expected_conditions.presence_of_element_located((by, value))
            )
            return True
        except TimeoutException:
            return False

    def is_element_exist_by_resource_id(
            self,
            resource_id: str,
            wait_for_duration: int = 15
    ):
        try:
            WebDriverWait(self.driver, wait_for_duration).until(
                expected_conditions.presence_of_element_located(
                    (AppiumBy.ANDROID_UIAUTOMATOR, f'new UiSelector().resourceId("{resource_id}")'))
            )
            return True
        except TimeoutException:
            return False

    def get_element(self, by: str = By, value: str | dict | None = None) -> WebElement:
        with allure.step(f'Getting element by: AppiumBy.{by} with locator: {value}'):
            if not self.is_element_exist(by, value):
                raise Exception('Element not found')
            return self.driver.find_element(by, value)

    def get_element_by_resource_id(self, resource_id: str, wait_for_duration: int = 15) -> WebElement:
        with allure.step(f'Getting element by resource-id: {resource_id}'):
            if not self.is_element_exist_by_resource_id(resource_id, wait_for_duration):
                raise Exception(f'Element with resource ID "{resource_id}" not found')
            return self.driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                                            f'new UiSelector().resourceId("{resource_id}")')

    def click_element_by_id(self, locator):
        with allure.step(f'Clicking element by locator: {locator}'):
            self.get_element_by_resource_id(locator).click()

    def click_element_by_xpath(self, locator):
        with allure.step(f'Clicking element by locator: {locator}'):
            self.get_element(AppiumBy.XPATH, locator).click()

    def send_keys_to_element(self, locator, text):
        with allure.step(f'Sending keys to element with locator: {locator}'):
            element = self.get_element_by_resource_id(locator)
            element.click()
            element.send_keys(text)

    def send_keys_to_element_by_xpath(self, locator, text):
        with allure.step(f'Sending keys to element with locator: {locator}'):
            element = self.get_element(AppiumBy.XPATH, locator)
            element.click()
            element.send_keys(text)

    def find_webview_context(self):
        with allure.step('Finding webview context'):
            contexts = self.driver.contexts
            time.sleep(5)
            return next((x for x in contexts if x.startswith('WEBVIEW')), None)

    def switch_to_webview(self, webview_id: str = None) -> None:
        with allure.step(f'Switching to webview, webview_id: {webview_id}'):
            webview = webview_id if webview_id is not None else self.find_webview_context()
            self.driver.switch_to.context(webview)

    def switch_to_flutter(self) -> None:
        with allure.step('Switching to flutter'):
            self.driver.switch_to.context('FLUTTER')

    def switch_to_native(self) -> None:
        with allure.step('Switching to native'):
            self.driver.switch_to.context('NATIVE_APP')

    def enter_key(self, key: int) -> None:
        with allure.step(f'Entering software keyboard key: {key}'):
            self.driver.press_keycode(key)

    def hide_keyboard(self):
        with allure.step('Hiding software keyboard after data input'):
            try:
                self.driver.hide_keyboard()
                print("Keyboard hidden successfully.")
            except Exception as e:
                print("Failed to hide keyboard:", str(e))

    def scroll_gesture(self, start_x, start_y, end_x, end_y, duration):
        with allure.step(f'Scrolling to element from positions: {start_x}, {start_y} to {end_x}, {end_y}'):
            params = {
                'start_x': start_x,
                'start_y': start_y,
                'end_x': end_x,
                'end_y': end_y,
                'duration': duration
            }
            self.driver.swipe(params['start_x'], params['start_y'], params['end_x'], params['end_y'],
                              params['duration'])
