from ui.pages.base_page import BasePage


class ProofOfIdentityPage(BasePage):
    start_verification_button = 'startVerificationButton'
    exit_button = '//android.widget.TextView[@resource-id="com.axevil.mobile.dev:id/button_text" and @text="Exit"]'
    exit_verification_button = 'closeAppBarButton'
    portfolio_idea_button = '//android.view.View[contains(@content-desc, "Stripe")]'
    that_is_correct_button = 'thatIsCorrectButton'
    continue_button = 'continueVerificationButton'

    def click_start_verification_button(self):
        self.click_element_by_id(self.start_verification_button)

    def click_exit_veriff_button(self):
        self.click_element_by_xpath(self.exit_button)

    def click_exit_verification_button(self):
        self.click_element_by_id(self.exit_verification_button)

    def click_portfolio_idea_button(self):
        self.click_element_by_xpath(self.portfolio_idea_button)

    def click_that_is_correct_button(self):
        self.click_element_by_id(self.that_is_correct_button)

    def click_continue_button(self):
        self.click_element_by_id(self.continue_button)
