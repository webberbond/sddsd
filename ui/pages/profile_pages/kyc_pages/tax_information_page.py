from appium.webdriver.extensions.android.nativekey import AndroidKey

from ui.pages.base_page import BasePage


class TaxInformationPage(BasePage):
    country_of_residence_button = 'countryOfResidenceInputField'
    my_country_button = '//android.view.View[contains(@content-desc, "Uzbekistan")]'
    state_input_field = 'stateOrRegionInputField'
    city_input_field = 'cityInputField'
    address_input_field = 'addressLine1InputField'
    zip_code_input_field = 'zipOrPostalCodeInputField'
    continue_button = 'continueVerificationButton'
    country_of_tax_residence_button = 'countryOfTaxResidenceInputField'
    tax_identification_number_input_field = 'taxIdentificationNumberInputField'
    great_news_button = 'greatNewsButton'

    def click_country_of_residence_button(self):
        self.click_element_by_id(self.country_of_residence_button)

    def input_letter_u(self):
        self.enter_key(AndroidKey.U)

    def input_letter_z(self):
        self.enter_key(AndroidKey.Z)

    def input_letter_b(self):
        self.enter_key(AndroidKey.B)

    def click_my_country_button(self):
        self.click_element_by_xpath(self.my_country_button)

    def send_keys_to_state_input_field(self, state):
        self.send_keys_to_element(self.state_input_field, state)

    def send_keys_to_city_input_field(self, city):
        self.send_keys_to_element(self.city_input_field, city)

    def send_keys_to_address_input_field(self, address_input):
        self.send_keys_to_element(self.address_input_field, address_input)

    def send_keys_to_zip_code_input_field(self, zip_code):
        self.send_keys_to_element(self.zip_code_input_field, zip_code)

    def click_continue_button(self):
        self.click_element_by_id(self.continue_button)

    def click_country_of_tax_residence_button(self):
        self.click_element_by_id(self.country_of_tax_residence_button)

    def send_keys_to_tax_identification_number_input_field(self, tax_identification_number):
        self.send_keys_to_element(self.tax_identification_number_input_field, tax_identification_number)

    def click_great_news_button(self):
        self.click_element_by_id(self.great_news_button)
