from ui.pages.base_page import BasePage


class ProfilePage(BasePage):
    verification_button = 'verificationButton'

    def click_verification_button(self):
        self.click_element_by_id(self.verification_button)
