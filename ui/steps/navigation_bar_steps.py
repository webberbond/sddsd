import allure

from ui.pages.navigation_bar import NavigationBar


class NavigationBarSteps:
    def __init__(self, driver):
        self.navigation_bar = NavigationBar(driver)

    def open_investment_page(self):
        with allure.step('Opening investment page'):
            self.navigation_bar.click_home_navigation_bar_button()

    def open_portfolio_page(self):
        with allure.step('Opening portfolio page'):
            self.navigation_bar.click_portfolio_navigation_bar_button()

    def open_news_page(self):
        with allure.step('Opening news page'):
            self.navigation_bar.click_news_navigation_bar_button()

    def open_profile_page(self):
        with allure.step('Opening profile page'):
            self.navigation_bar.click_profile_navigation_bar_button()
