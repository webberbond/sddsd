import allure

from ui.pages.portfolio_pages.portfolio_page import PortfolioPage


class PortfolioPageSteps:
    def __init__(self, driver):
        self.portfolio_page = PortfolioPage(driver)

    def open_stripe_order_in_portfolio_page(self):
        with allure.step('Updating order status using UI'):
            self.portfolio_page.click_close_modal_window_button()
            self.portfolio_page.click_stripe_order_button()
