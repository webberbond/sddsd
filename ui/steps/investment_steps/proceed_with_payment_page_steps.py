import allure

from ui.pages.investment_pages.proceed_with_payment_page import ProceedWithPaymentPage


class ProceedWithPaymentPageSteps:
    def __init__(self, driver):
        self.proceed_with_payment_page = ProceedWithPaymentPage(driver)

    def proceed_with_payment(self):
        with allure.step('Clicking "Proceed with payment" button'):
            self.proceed_with_payment_page.click_proceed_with_payment_button()
