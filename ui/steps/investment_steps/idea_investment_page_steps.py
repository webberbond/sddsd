import allure

from ui.pages.investment_pages.idea_investment_page import IdeaInvestmentPage


class IdeaInvestmentPageSteps:
    def __init__(self, driver):
        self.idea_investment_page = IdeaInvestmentPage(driver)

    def click_invest_button(self):
        with allure.step('Clicking "Invest" button'):
            self.idea_investment_page.click_invest_button()
