import allure

from ui.pages.investment_pages.bank_transfer_page import BankTransferPage


class BankTransferPageSteps:
    def __init__(self, driver):
        self.bank_transfer_page = BankTransferPage(driver)

    def proceed_with_payment(self):
        with allure.step('Scrolling to "Sent the funds" button, clicking button, navigating to portfolio'):
            self.bank_transfer_page.scroll_to_sent_the_funds_button()
            self.bank_transfer_page.scroll_to_sent_the_funds_button()
            self.bank_transfer_page.click_sent_the_funds_button()
            self.bank_transfer_page.click_go_to_portfolio_button()
