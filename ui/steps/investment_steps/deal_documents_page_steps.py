import allure

from ui.pages.investment_pages.deal_documents_page import DealDocumentsPage


class DealDocumentsPageSteps:
    def __init__(self, driver):
        self.deal_documents_page = DealDocumentsPage(driver)

    def sign_documents(self):
        with allure.step('Ticking checkbox, clicking "Sign Documents" button'):
            self.deal_documents_page.click_checkbox_documents_button()
            self.deal_documents_page.click_sign_documents_button()
