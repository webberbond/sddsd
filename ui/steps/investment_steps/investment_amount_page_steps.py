import allure

from ui.pages.investment_pages.investment_amount_page import InvestmentAmountPage


class InvestmentAmountPageSteps:
    def __init__(self, driver):
        self.investment_amount_page = InvestmentAmountPage(driver)

    def proceed_with_amount(self, amount):
        with allure.step('Inputting investment amount, clicking "Continue" button'):
            self.investment_amount_page.send_keys_to_investment_amount_input_field(amount)
            self.investment_amount_page.hide_keyboard()
            self.investment_amount_page.click_continue_button()
