import allure

from ui.pages.investment_pages.payment_page import PaymentPage


class PaymentPageSteps:
    def __init__(self, driver):
        self.payment_page = PaymentPage(driver)

    def choose_bank(self, payment_method):
        with allure.step('Clicking "Swift" button'):
            if payment_method == "Bank":
                self.payment_page.click_swift_button()
            else:
                print('Not supported yet')
