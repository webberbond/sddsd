import allure

from ui.pages.login_pages.otp_page import OtpPage


class OtpPageSteps:
    def __init__(self, driver):
        self.otp_page = OtpPage(driver)

    def input_confirmation_code(self):
        with allure.step('Entering pin code'):
            self.otp_page.click_first_confirmation_code_digit()
            self.otp_page.click_second_confirmation_code_digit()
            self.otp_page.click_third_confirmation_code_digit()
            self.otp_page.click_fourth_confirmation_code_digit()
