import allure

from ui.pages.login_pages.register_page import RegisterPage


class RegisterPageSteps:
    def __init__(self, driver):
        self.register_page = RegisterPage(driver)

    def register(self, user):
        with allure.step('Inputting user name, user email, clicking "Create Account" button'):
            self.register_page.send_keys_to_user_name_input_field(user.user_name)
            self.register_page.send_keys_to_user_email_input_field(user.user_email)
            self.register_page.hide_keyboard()
            self.register_page.click_create_account_button()
