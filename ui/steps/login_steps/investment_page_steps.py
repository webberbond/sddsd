import allure

from ui.pages.investment_pages.investment_page import InvestmentPage


class InvestmentPageSteps:
    def __init__(self, driver):
        self.investment_page = InvestmentPage(driver)

    def clear_screen(self):
        with allure.step('Closing story and modal window'):
            self.investment_page.click_close_story_button()
            self.investment_page.click_close_modal_window_button()

    def open_idea_to_invest(self, idea_id):
        with allure.step('Opening idea to invest'):
            self.investment_page.click_idea_to_invest_button(idea_id)
