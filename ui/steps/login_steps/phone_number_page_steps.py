import allure

from ui.pages.login_pages.phone_number_page import PhoneNumberPage


class PhoneNumberPageSteps:
    def __init__(self, driver):
        self.phone_number_page = PhoneNumberPage(driver)

    def choose_country(self, user):
        with allure.step('Choosing country'):
            self.phone_number_page.click_country_dropdown_button()
            self.phone_number_page.send_keys_to_country_search_field(user.country_name)
            self.phone_number_page.click_my_country_button()

    def send_code(self, user):
        with allure.step('Inputting phone number and clicking "Send Code" button'):
            self.phone_number_page.send_keys_to_phone_number_input_field(user.user_phone_number)
            self.phone_number_page.click_send_code_button()
