import allure

from ui.pages.login_pages.onboarding_page import OnboardingPage


class OnboardingPageSteps:
    def __init__(self, driver):
        self.onboarding_page = OnboardingPage(driver)

    def click_onboarding_buttons(self):
        with allure.step('Click onboarding buttons'):
            self.onboarding_page.click_onboarding_next_button1()
            self.onboarding_page.click_onboarding_next_button2()
            self.onboarding_page.click_onboarding_next_button3()
