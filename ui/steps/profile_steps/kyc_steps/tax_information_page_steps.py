import time

import allure

from ui.entities.datafaker.account_model_data_faker import AccountModelDataFaker
from ui.pages.profile_pages.kyc_pages.tax_information_page import TaxInformationPage


class TaxInformationPageSteps():
    def __init__(self, driver):
        self.tax_information_page = TaxInformationPage(driver)
        self.user = AccountModelDataFaker()

    def enter_country_name(self):
        with allure.step('Choosing country'):
            time.sleep(1)
            self.tax_information_page.input_letter_u()
            self.tax_information_page.input_letter_z()
            self.tax_information_page.input_letter_b()
            self.tax_information_page.click_my_country_button()

    def proceed_verification(self, user):
        with allure.step('Proceeding verification'):
            self.tax_information_page.click_country_of_residence_button()
            self.enter_country_name()
            self.tax_information_page.send_keys_to_state_input_field(user.state)
            self.tax_information_page.hide_keyboard()
            self.tax_information_page.send_keys_to_city_input_field(user.city)
            self.tax_information_page.hide_keyboard()
            self.tax_information_page.send_keys_to_address_input_field(user.address)
            self.tax_information_page.hide_keyboard()
            self.tax_information_page.send_keys_to_zip_code_input_field(user.zip_code)
            self.tax_information_page.hide_keyboard()
            self.tax_information_page.click_continue_button()
            self.tax_information_page.click_country_of_tax_residence_button()
            self.enter_country_name()
            self.tax_information_page.send_keys_to_tax_identification_number_input_field(user.tax_identification_number)
            self.tax_information_page.hide_keyboard()
            self.tax_information_page.click_continue_button()
            self.tax_information_page.click_great_news_button()
