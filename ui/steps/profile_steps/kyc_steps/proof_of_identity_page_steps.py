import allure

from ui.pages.profile_pages.kyc_pages.proof_of_identity_page import ProofOfIdentityPage


class ProofOfIdentityPageSteps():
    def __init__(self, driver):
        self.proof_of_identity_page = ProofOfIdentityPage(driver)

    def start_verification(self):
        with allure.step('Starting verification'):
            self.proof_of_identity_page.click_start_verification_button()
            self.proof_of_identity_page.click_exit_veriff_button()
            self.proof_of_identity_page.click_exit_verification_button()

    def continue_verification(self):
        with allure.step('Continuing verification'):
            self.proof_of_identity_page.click_that_is_correct_button()
            self.proof_of_identity_page.click_continue_button()
