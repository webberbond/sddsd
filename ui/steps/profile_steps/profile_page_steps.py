import allure

from ui.pages.profile_pages.profile_page import ProfilePage


class ProfilePageSteps:
    def __init__(self, driver):
        self.profile_page = ProfilePage(driver)

    def open_verification_page(self):
        with allure.step('Opening verification page'):
            self.profile_page.click_verification_button()
