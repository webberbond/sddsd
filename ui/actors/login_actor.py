from ui.actors.kyc_actor import KycActor
from ui.steps.login_steps.investment_page_steps import InvestmentPageSteps
from ui.steps.login_steps.onboarding_page_steps import OnboardingPageSteps
from ui.steps.login_steps.otp_page_steps import OtpPageSteps
from ui.steps.login_steps.phone_number_page_steps import PhoneNumberPageSteps
from ui.steps.login_steps.register_page_steps import RegisterPageSteps


class LoginActor:
    def __init__(self, driver):
        self.onboarding_page_steps = OnboardingPageSteps(driver)
        self.phone_number_page_steps = PhoneNumberPageSteps(driver)
        self.otp_page_steps = OtpPageSteps(driver)
        self.register_page_steps = RegisterPageSteps(driver)
        self.investment_page_steps = InvestmentPageSteps(driver)
        self.kyc_actor = KycActor(driver)

    def create_account(self, user):
        self.onboarding_page_steps.click_onboarding_buttons()
        self.phone_number_page_steps.choose_country(user)
        self.phone_number_page_steps.send_code(user)
        self.otp_page_steps.input_confirmation_code()
        self.register_page_steps.register(user)
        self.investment_page_steps.clear_screen()

        return self.kyc_actor
