from ui.steps.portfolio_steps.portfolio_page_steps import PortfolioPageSteps


class PortfolioActor:
    def __init__(self, driver):
        self.portfolio_page_steps = PortfolioPageSteps(driver)

    def open_stripe_order_page(self):
        self.portfolio_page_steps.open_stripe_order_in_portfolio_page()
