from ui.actors.investment_actor import InvestmentActor
from ui.steps.login_steps.investment_page_steps import InvestmentPageSteps
from ui.steps.navigation_bar_steps import NavigationBarSteps


class IdeasActor:
    def __init__(self, driver):
        self.navigation_bar_steps = NavigationBarSteps(driver)
        self.investment_page_steps = InvestmentPageSteps(driver)
        self.investment_actor = InvestmentActor(driver)

    def invest_in(self, idea_id):
        self.navigation_bar_steps.open_investment_page()
        self.investment_page_steps.open_idea_to_invest(idea_id)

        return self.investment_actor
