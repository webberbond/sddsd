from ui.actors.ideas_actor import IdeasActor
from ui.steps.navigation_bar_steps import NavigationBarSteps
from ui.steps.profile_steps.kyc_steps.proof_of_identity_page_steps import ProofOfIdentityPageSteps
from ui.steps.profile_steps.kyc_steps.tax_information_page_steps import TaxInformationPageSteps
from ui.steps.profile_steps.profile_page_steps import ProfilePageSteps


class KycActor:
    def __init__(self, driver):
        self.navigation_bar_steps = NavigationBarSteps(driver)
        self.profile_page_steps = ProfilePageSteps(driver)
        self.proof_of_identity_page_steps = ProofOfIdentityPageSteps(driver)
        self.tax_information_page_steps = TaxInformationPageSteps(driver)
        self.ideas_actor = IdeasActor(driver)

    def proceed_kyc(self, user):
        self.navigation_bar_steps.open_profile_page()
        self.profile_page_steps.open_verification_page()
        self.proof_of_identity_page_steps.start_verification()
        self.profile_page_steps.open_verification_page()
        self.proof_of_identity_page_steps.continue_verification()
        self.tax_information_page_steps.proceed_verification(user)

        return self.ideas_actor
