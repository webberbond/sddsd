import unittest

from ui.steps.investment_steps.bank_transfer_page_steps import BankTransferPageSteps
from ui.steps.investment_steps.deal_documents_page_steps import DealDocumentsPageSteps
from ui.steps.investment_steps.idea_investment_page_steps import IdeaInvestmentPageSteps
from ui.steps.investment_steps.investment_amount_page_steps import InvestmentAmountPageSteps
from ui.steps.investment_steps.payment_page_steps import PaymentPageSteps
from ui.steps.investment_steps.proceed_with_payment_page_steps import ProceedWithPaymentPageSteps
from ui.steps.portfolio_steps.portfolio_page_steps import PortfolioPageSteps


class InvestmentActor:
    def __init__(self, driver):
        self.idea_investment_page_steps = IdeaInvestmentPageSteps(driver)
        self.investment_amount_page_steps = InvestmentAmountPageSteps(driver)
        self.deal_documents_page_steps = DealDocumentsPageSteps(driver)
        self.proceed_with_payment_page_steps = ProceedWithPaymentPageSteps(driver)
        self.payment_page_steps_steps = PaymentPageSteps(driver)
        self.bank_transfer_page_steps = BankTransferPageSteps(driver)
        self.portfolio_page_steps = PortfolioPageSteps(driver)

        self.assertTrue = unittest.TestCase().assertTrue

    def invest_with_commission_included(self, amount):
        self.idea_investment_page_steps.click_invest_button()
        self.investment_amount_page_steps.proceed_with_amount(amount)

        return self

    def check_spv_document(self):
        self.assertTrue(self.deal_documents_page_steps.deal_documents_page.is_spv_agreement_button_visible())
        self.deal_documents_page_steps.sign_documents()

        return self

    def proceed_to_payment(self):
        self.proceed_with_payment_page_steps.proceed_with_payment()

        return self

    def select_payment_method(self, payment_method):
        self.payment_page_steps_steps.choose_bank(payment_method)

        return self

    def confirm_bank_payment(self):
        self.bank_transfer_page_steps.proceed_with_payment()

        return self

