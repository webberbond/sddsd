import logging
import os

from appium.options.common import AppiumOptions
from appium.webdriver import Remote
from appium.webdriver import webdriver

from ui.config.config import config

desired_caps = {}


class DriverManager:

    def get_driver(self) -> webdriver.WebDriver:
        platform_name = None
        platform_name_env = os.getenv('PLATFORM_NAME')
        if platform_name_env:
            platform_name = platform_name_env.lower()
        else:
            logging.error('Environment variable is not available')

        if platform_name == 'android':
            desired_caps['platformName'] = config.ANDROID_PLATFORM_NAME
            desired_caps['automationName'] = config.AUTOMATION_NAME
            desired_caps['app'] = config.ANDROID_APP_LOCATION
        elif platform_name == 'ios':
            desired_caps['platformName'] = config.IOS_PLATFORM_NAME
            desired_caps['automationName'] = config.AUTOMATION_NAME
            desired_caps['app'] = config.IOS_APP_LOCATION
        else:
            logging.error('Error occurred when trying to get driver')

        return Remote(config.APPIUM_URL, options=AppiumOptions().load_capabilities(desired_caps))
