from dataclasses import dataclass


@dataclass
class AccountModel:
    def __init__(self, country_name: str, user_phone_number: int, user_name: str, user_email: str, state: str,
                 city: str, address: str,
                 zip_code: str, tax_identification_number):
        self.country_name = country_name
        self.user_phone_number = user_phone_number
        self.user_name = user_name
        self.user_email = user_email
        self.state = state
        self.city = city
        self.address = address
        self.zip_code = zip_code
        self.tax_identification_number = tax_identification_number
