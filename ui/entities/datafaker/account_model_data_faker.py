import factory
import faker
from pytest_factoryboy import register

from ui.entities.models.account_model import AccountModel

fake = faker.Faker()


@register
class AccountModelDataFaker(factory.Factory):
    class Meta:
        model = AccountModel

    country_name = 'Uzbekistan'
    user_phone_number = factory.LazyFunction(lambda: '90' + str(fake.unique.random_int(min=1000000, max=9999999)))
    user_name = factory.Faker('name')
    user_email = factory.Faker('email')
    state = factory.Faker('state')
    city = factory.Faker('city')
    address = factory.Faker('address')
    zip_code = factory.Faker('zipcode')
    tax_identification_number = factory.LazyFunction(lambda: fake.random_int(min=100000000, max=999999999))


class UserFabric:
    @staticmethod
    def get_new_random_user():
        return AccountModelDataFaker.create()
