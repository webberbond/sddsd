import os


class Config:
    APPIUM_URL = 'http://0.0.0.0:4723'

    ANDROID_PLATFORM_NAME = 'android'
    IOS_PLATFORM_NAME = 'ios'

    ANDROID_PLATFORM_VERSION = '12'
    IOS_PLATFORM_VERSION = '17.2'

    ANDROID_DEVICE_NAME = 'Android Test PIXEL_5'
    IOS_DEVICE_NAME = 'iOS Test IPHONE_15_PRO'

    AUTOMATION_NAME = 'UiAutomator2'

    ANDROID_APP_LOCATION = os.path.join(os.path.dirname(os.path.dirname(__file__)), "apps", "app-dev-debug.apk")
    IOS_APP_LOCATION = os.path.join(os.path.dirname(os.path.dirname(__file__)), "apps", "Runner.ipa")

    API_URL = os.getenv('API_URL')

    PASSWORD = os.getenv('ADMIN_PASSWORD')
    EMAIL = os.getenv('ADMIN_EMAIL')


config = Config()
